#!/usr/bin/env python
# https://github.com/rg3/youtube-dl/blob/master/README.md#embedding-youtube-dl
import os
import youtube_dl
import json
import sys

from rq import get_current_job



class Downloader:
    def __init__(self, url):
        self.job = get_current_job()
        self.url = url

    def debug(self, msg):
        if msg.startswith('{"id"'):
            self.job.meta['info'] = json.loads(msg)

    def warning(self, msg):
        pass

    def error(self, msg):
        # model.update(job, status=model.STATE_ERROR, error = error)
        pass

    def get_hook(self):
        def my_hook(d):
            if d['status'] == 'downloading' and d['total_bytes'] is not None:
                p = d['downloaded_bytes'] / d['total_bytes']
                self.job.meta['perc'] = p
                self.job.save_meta()
        return my_hook

    def run(self):
        # model.update(job, status=model.STATE_RUNNING)
        opts = {
            'logger': self,
            'to_stdout': True,
            'dump_single_json': True,
            'progress_hooks': [self.get_hook()],
            'outtpl': '%(title)s-%(id)s.%(ext)s'
        }
        with youtube_dl.YoutubeDL(opts) as ydl:
            ydl.download([self.url])
        
        return self.url


def download(url):
    return Downloader(url).run()



