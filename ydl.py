#!/usr/bin/env python
from os import path

from bottle import route, run, template
from bottle import get, post, request
from bottle import static_file

from shared import jobs

from rq import Connection, Queue
from rq.job import Job
from rq.registry import FinishedJobRegistry, StartedJobRegistry

def job_to_api(j):
    d = j.to_dict()
    d['id'] = j.id
    d['data'] = ""
    d['meta'] = j.meta
    d['exc_info'] = j.exc_info
    d['result'] = j.result
    return d


## api
@get('/api/v1/job')
def get_jobs():
    queues = Queue.all()
    data = []
    for q in queues:
        data = data + [ job_to_api(j) for j in q.jobs ]

    running_job_registry = FinishedJobRegistry()
    data = data + [ job_to_api(Job.fetch(id)) for id in running_job_registry.get_job_ids() ]

    finished_job_registry = StartedJobRegistry()
    data = data + [ job_to_api(Job.fetch(id)) for id in finished_job_registry.get_job_ids() ]
        
    return {
        'data' : data
    }


@post('/api/v1/job')
def create_jobs():
    q = Queue()
    url = request.forms.get('url')
    print("#### CREATE JOB '{}'".format(url))
    job = q.enqueue(jobs.download, url, ttl=-1, result_ttl=-1)
    job.meta['url'] = url
    job.save_meta()
    return job_to_api(job)

@get('/api/v1/job/<id>')
def get_job(id):
    job = Job.fetch(id)
    return job_to_api(job)

@route('/api/v1/job/<id>', method='DELETE' )
def delete_job(id):
    job = Job.fetch(id)
    job.delete()
    return job_to_api(job)

## frontend

@get('/static/<filename:path>')
def static_assets(filename):
    return static_file(filename, root='static/')


@get('/')
def index():
    return template('base')


if __name__ == "__main__":
    with Connection():
        run(host='localhost', port=8123, reloader=True, debug=True)

