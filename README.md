**VWIP** : Very Work In Progress

## requirements

- python3
- youtube-dl
- redis
- pipenv

It should work with virtualenv, a `requirements.txt` file is provided. Not tested.

## Setup

    $ pipenv --python 3  --site-packages
    $ pipen install

## Run

in one terminal, run the web service

    $ pipenv shell
    (env)$ ./ydl.py

in another terminal, run the worker

    $ pipenv shell
    (env)$ rd