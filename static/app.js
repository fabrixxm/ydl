// Model
var Jobs = {
    list: [],
    
    update: () => {
        m.request("/api/v1/job").then((result) => {
            Jobs.list = result.data;
        });
    },
    
    add: (url) => {
        var data = new FormData();
        data.append("url", url);
        
        m.request({
            url: "/api/v1/job",
            method: "POST",
            data: data
        }).then(Jobs.update);
    },
    
    delete: (job) => {
        m.request({
            url: "/api/v1/job/" + job.id,
            method: "DELETE",
        }).then(Jobs.update);
    }
};


// Views
var ListBoxRow = {
    view: (vnode) => {
        var job = vnode.attrs.job;
        var image = m("img", { src: "/static/icons/video-x-generic.svg" });
        var content = [];
        content.push(m("dt", job.meta.url));

        /*if (job.status == "waiting" || job.status == "done") {
            image = m("img.icon", { style : "background-image: url(" + job.thumb + ");" });
            content.push(m("dd", job.url));
        }*/
        if (job.status == "running" || job.status == "started") {
            // image = m(".icon", { style : "background-image: url(" + job.thumb + ");" });
            content.push(m("dd", m("progress.fill", {value:job.meta.perc})));
        }
        
        
        var body = [
            image,
            m("dl", content)
        ]
        
        if (job.status == "finished") {
            body.push(m("img.icon", { src: "/static/icons/emblem-ok-symbolic.svg" }));
        }
        
        body.push(m("button.icon.VTKButton", { onclick: () => Jobs.delete(job)}, "x"));
        
        return m("li.VTKListBoxRow", body);

    },
};

var ListBox = {
    view: () => {
        return m("ul.VTKColumn.VTKListBox", Jobs.list.map(function(job) {
            return m(ListBoxRow, { job: job })
        }))
    },
};


// setup mithril
var mainpanel = document.getElementById("mainpanel");
m.mount(mainpanel, ListBox);



// setup vanilla 
document.getElementById("btnAdd").addEventListener("click", ()=>{
    document.getElementById("dlgAdd").classList.remove("hidden");
})
document.querySelectorAll(".action-close").forEach((e)=>{
    e.addEventListener("click", (evt)=>{
        evt.preventDefault();
        document.getElementById("dlgAdd").classList.add("hidden");
    });
});
document.getElementById("btnSubmit").addEventListener("click", ()=>{
    var url = document.getElementById("url").value;
    console.log("url", url);
    Jobs.add(url);
    document.getElementById("dlgAdd").classList.add("hidden");
})


// start
Jobs.update();
setInterval(Jobs.update, 1000);





